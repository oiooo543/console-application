const inquirer   = require('inquirer');
const files      = require('./files');
const packageJson = require('../package.json');
const Configstore = require('configstore');

module.exports = {

    askCredentials: () => {
        const config = new Configstore(packageJson.name);
        const questions = [
            {
                name: 'username',
                type: 'input',
                message: 'Enter your user name:',
                validate: function( value ) {
                    if (value.length) {
                        return true;
                    } else {
                        return 'Please enter your username.';
                    }
                }
            }
        ];
        return inquirer.prompt(questions);
    },
}
