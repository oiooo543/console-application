const axios = require('axios');
const files = require('./files');
const generate  = require('../bin/generate');
const menu = require('./menu');
const CLI = require('clui');
const Configstore = require('configstore');
const packageJson = require('../package.json');

module.exports = {

    requestSecretMessage: async ()=> {


        const config = new Configstore(packageJson.name);
        const url = config.get('url');
        const signed = await generate.signMessage(config.get('username'));
        const username = {"raw": config.get('username'), 'signed': signed}
        const secreteName = await menu.requestSecretName();
        const secretName = secreteName['secretName'];
        const signedSecretName = await generate.signMessage(secretName);


        Spinner = CLI.Spinner;
        const status = new Spinner('Getting your message, please wait...\r\n');
        status.start();
        try {
            const response = await axios.get(url + '/getSecret', {
                headers: {
                    usernameRaw: username['raw'],
                    signedUsername: signed,
                    secretName: secretName,
                    signedSecretName: signedSecretName,
                }
            });

           /* console.log(response['data']);
            console.log('successful');*/


            const message = await generate.decreptMessage(response['data']);
            console.log('The Message is: ' + message);
            return message


        } catch (error) {
            console.error(error);

        } finally {
            status.stop();
        }
    },
}
