const inquirer   = require('inquirer');
const files      = require('./files');

module.exports = {

    askStartMenu: () => {
        const questions = [
            {
                name: 'choice',
                type: 'input',
                message: 'Please enter 1 to get message or 2 to send another:',
                validate: function( value ) {
                    if (value.length) {
                        return true;
                    } else {
                        return 'Please enter a menu.';
                    }
                }
            }
        ];
        return inquirer.prompt(questions);
    },

    requestSecretName: () => {
        const questions = [
            {
                name: 'secretName',
                type: 'input',
                message: 'please enter the secret name: ',
                validate: function( value ) {
                    if (value.length) {
                        return true;
                    } else {
                        return 'Please enter your secrete name.';
                    }
                }
            }
        ];
        return inquirer.prompt(questions);
    },


}
