const files = require('./files');
const { generateKeyPairSync } = require('crypto');
const packageJson = require('../package.json');
const Configstore = require('configstore');
const crypto = require('crypto');
const axios = require('axios');
var constants = require("constants");



module.exports = {

    generateKey: () => {


    return   { publicKey, privateKey } = generateKeyPairSync('rsa', {
            modulusLength: 2048,
            publicKeyEncoding: {
                type: 'spki',
                format: 'pem'
            },
            privateKeyEncoding: {
                type: 'pkcs8',
                format: 'pem',

            }

      });
    },

    encryptMessage: (message) => {
        const config = new Configstore(packageJson.name);
      //  const algorithm = 'aes-256-cbc';
       const key = config.get('serverKey');
       const buffer = Buffer.from(message);
       const  encrypted = crypto.publicEncrypt(key, buffer);

      // console.log(module.exports.decreptMessage(encrypted.toString('base64')));
        return { encryptedData: encrypted.toString('base64')};

    },

    signMessage: (message) => {
        console.log(message);
        const config = new Configstore(packageJson.name);
        const sign = crypto.createSign('SHA256');
        sign.update(message);
        sign.end();
        const signature = sign.sign(config.get('privateKey'),'base64');

        const verify = crypto.createVerify('SHA256');
        verify.update(message);
        verify.end();
        console.log(config.get('publicKey'));
        console.log('Its working: ' + verify.verify(config.get('publicKey'), signature, 'base64'));
        return signature;
    },

    decreptMessage: (message) =>{

        const config = new Configstore(packageJson.name);
        const key = config.get('privateKey');
        //console.log(key);
        var b = new Buffer.from(message, 'base64');
        const decryptedMessage = crypto.privateDecrypt(key, b);
        return decryptedMessage.toString("ascii");

    }

}



