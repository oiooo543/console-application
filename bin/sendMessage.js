const inquirer   = require('inquirer');
const files      = require('./files');
const NodeRSA = require('node-rsa');


module.exports = {

    getMessageDetails: () => {
        const questions = [
            {
                name: 'username',
                type: 'input',
                message: 'Enter your user name:',
                validate: function( value ) {
                    if (value.length) {
                        return true;
                    } else {
                        return 'Please enter your username.';
                    }
                }
            },
            {
                name: 'secretName',
                type: 'input',
                message: 'Enter Secrete Name:',
                validate: function(value) {
                    if (value.length) {
                        return true;
                    } else {
                        return 'Please enter your password.';
                    }
                }
            },

            {
                name: 'secretMessage',
                type: 'input',
                message: 'Enter Secrete Message:',
                validate: function(value) {
                    if (value.length) {
                        return true;
                    } else {
                        return 'Please enter your password.';
                    }
                }
            }
        ];
        return inquirer.prompt(questions);
    },


}
