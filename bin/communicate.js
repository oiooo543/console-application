const axios = require('axios');
const files      = require('./files');
const Configstore = require('configstore');
const inquirer  = require('../bin/inquirer');
const generate  = require('../bin/generate');
const packageJson = require('../package.json');
const crypto = require('crypto');
const CLI = require('clui');

const sendmessage = require('./sendMessage');

module.exports = {

     sendRegistrationRequest : async () =>{

        const config = new Configstore(packageJson.name);
        const url = config.get('url');



        try {
            const response = await axios.post(url + '/register', {
                username: config.get('username'),
                publicKey: config.get('publicKey'),
            });
            console.log('Registration Successful');
        } catch (error) {
            console.error('Registration Failed: I think you already registered');
        }

   },

   intializer: async ()=> {


       const config = new Configstore(packageJson.name);
       const url = config.get('url');

            if(!config.get('publicKey') || !config.get('privateKey')) {
                const keys = await generate.generateKey();
                console.log(keys);
                config.set('publicKey', keys['publicKey']);
                config.set('privateKey', keys['privateKey']);
                config.set('url', 'http://localhost:8000/api/v1');
                const credentials = await inquirer.askCredentials();
                config.set('username', credentials['username']);
                console.log(config.get('username'));

               // const init = await intializer();

                const resp = await module.exports.sendRegistrationRequest();

                const serverkey = await module.exports.requestServerKey();
            }



   },

   requestServerKey: async ()=> {
       Spinner = CLI.Spinner;
       const status = new Spinner('Getting Server key, please wait...\r\n');
       status.start();
       const config = new Configstore(packageJson.name);
       const url = config.get('url');
        try {
            const response = await axios.get(url + '/getServerKey', {
                headers: {
                    username: config.get('username')
                }
            });
            console.log(response['data']);
            config.set('serverKey', response['data']);
            return response['data'];
        } catch (error) {
            console.error(error);
        } finally {
            status.stop();
        }
   },

   sendMessage : async (message) => {

       const config = new Configstore(packageJson.name);
       const url = config.get('url');

       try {
           const response = await axios.post(url + '/storemessage', message,);
           console.log('Secrete message saved successfully');
           return true;
       } catch (error) {
           console.error(error);
           return false;
       }
   },

   getMessage : async () => {
       const message = await sendmessage.getMessageDetails();
       console.log(message);

       message['encryptedMessage'] = await generate.encryptMessage(message['secretMessage']);

       console.log(message['encryptedMessage']['encryptedData']);

       message['encryptedMessage']['signature'] = await generate.signMessage(message['encryptedMessage']['encryptedData']);
       console.log(message);

       return message;
   }

}
